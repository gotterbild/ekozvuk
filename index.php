<?
	include_once 'lang.php';
	$l = getLang();
	if($_GET['debug'] === 'y')
		echo "Lang is ".$l."<br>";
?>
<!doctype html>
<html lang="<?=$l?>">
	<head>
		<meta charset="utf-8" />
		<meta property="og:image" content="//xn--80adcqbdeakeg9abfr7a1a5otb.xn--p1ai/wp-content/uploads/2015/01/Logo-meta.png"/>
		<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1"> <!-- обязательный параметр для адаптивных страниц -->

		<title><?=$t['title'][$l]?> | <?=$t['brand'][$l]?></title>

		<link rel="apple-touch-icon"      sizes="57x57"   href="./images/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon"      sizes="60x60"   href="./images/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon"      sizes="72x72"   href="./images/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon"      sizes="76x76"   href="./images/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon"      sizes="114x114" href="./images/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon"      sizes="120x120" href="./images/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon"      sizes="144x144" href="./images/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon"      sizes="152x152" href="./images/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon"      sizes="180x180" href="./images/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192" href="./images/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32"   href="./images/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96"   href="./images/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16"   href="./images/favicon/favicon-16x16.png">
		<link rel="manifest" href="images/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="./images/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">
		<link rel="canonical" href="http://ekozvuk.ru">
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
		<link rel="icon" href="favicon.ico" type="image/x-icon">

		<link rel="stylesheet" type="text/css" href="./css/reset.css">
		<link rel="stylesheet" type="text/css" href="./css/fonts.css">
		<link href='https://fonts.googleapis.com/css?family=Lobster&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="./css/style.css">

		<script type="text/javascript" src="./js/jquery-1.11.1.min.js"></script>

		<script type="text/javascript" src="./js/slick/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="./js/slick/slick.css"/>

		<?/*
		<script type="text/javascript" src="./js/fancybox/jquery.fancybox.pack.js"></script>
		<link rel="stylesheet" type="text/css" href="./js/fancybox/jquery.fancybox.css">
		*/?>

		<script type="text/javascript" src="./js/script.js"></script>
	</head>
	<body>
		<section id="intro" class="cover fixed noise darker shadow">
			<div class="top">
				<div class="container-grid">
					<a href="/" id="logo">
						<img src="images/logo-<?=$l?>.png" alt="<?=$t['brand'][$l]?>: <?=$t['slogan'][$l]?>">
					</a>
					<nav>
						<a href="#portfolio"><span><?=$t['portfolio'][$l]?></span></a>
						<a href="#services"><span><?=$t['services'][$l]?></span></a>
						<a href="#monials"><span><?=$t['monials'][$l]?></span></a>
						<a href="#questions"><span><?=$t['contact'][$l]?></span></a>
						<?//<a href="#"><span>Блог</span></a>?>
						<div class="clr"></div>
					</nav>
				</div>
			</div>
			<div class="container-grid content">
				<h1 class="light"><?=$t['intro1'][$l]?></h1>
				<div class="description t-center big light"><?=$t['intro2'][$l]?></div>
			</div>
			<a href="#questions" class="button fancybox red-bg white center long-shadow"><?=$t['order'][$l]?></a>

		</section>

		<div class="pos-rel"><span><img src="images/my-photo.png" alt="<?=$t['name'][$l]?>" class="myphoto"></span></div>

		<section id="aboutme" class="noise s-black-bg noise-fixed gray table thin">
			<div class="container-grid">
				<div class="content v-middle">
					<h4 class="center light"><?=$t['name'][$l]?></h4>
					<div class="pro light-blue center"><?=$t['titles'][$l]?></div>
					<div class="text"><?=$t['aboutme'][$l]?></div>
				</div>
			</div>
		</section>

		<section id="portfolio" class="wood-bg-dark shadow">
			<a href="#portfolio" class="header"><h2 class="h1-v2 dashed gray"><?=$t['myMixing'][$l]?></h2></a>
			<div class="continer-grid">
			<div class="portfolio">

				<article class="w25 light" >
					<div class="container cover noise darker" style="background-image: url('audio/letsfly-lonely-man2.jpg');">
						<div class="hashtag"><h4>#PopRock</h4></div>
						<div class="player">
							<div class="groupname">LetsFly</div>
							<div>«Lonely Man»</div>
							<br>
							<audio id="audio1" controls src="audio/letsfly-lonely-man.mp3">
								<?/*<source src="audio/letsfly-lonely-man.mp3" type='audio/mpeg; codecs="mp3"'>*/?>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/letsfly-lonely-man.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

				<article class="w25 light">
					<div class="container cover noise darker" style="background-image: url('audio/letsfly-take-the-moment2.jpg');">
						<div class="hashtag"><h4>#IndieRock</h4></div>
						<div class="player">
							<div class="groupname">LetsFly</div>
							<div>«Take the Moment»</div>
							<audio id="audio2" controls id="lf1">
								<source src="audio/letsfly-take-the-moment.mp3" type='audio/mpeg; codecs="mp3"'>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/letsfly-take-the-moment.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

				<article class="w25 light">
					<div class="container cover noise darker" style="background-image: url('audio/green-pint-igonish2.jpg');">
						<div class="hashtag"><h4>#Folk</h4></div>
						<div class="player">
							<div class="groupname">Зелёная Пинта</div>
							<div>«Igonish»</div>
							<audio id="audio3" controls>
								<source src="audio/green-pint-igonish.mp3" type='audio/mpeg; codecs="mp3"'>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/green-pint-igonish.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

				<article class="w25 light">
					<div class="container cover noise darker" style="background-image: url('audio/am-surah2.jpg');">
						<div class="hashtag"><h4>#progressive</h4></div>
						<div class="player">
							<div class="groupname">Am</div>
							<div>«Surah»</div>
							<audio id="audio4" controls>
								<source src="audio/am-surah.mp3" type='audio/mpeg; codecs="mp3"'>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/am-surah.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

				<article class="w25 light">
					<div class="container cover noise darker" style="background-image: url('audio/am-jazz2.jpg');">
						<div class="hashtag"><h4>#fusion</h4></div>
						<div class="player">
							<div class="groupname">Am</div>
							<div>«Jazz»</div>
							<audio id="audio5" controls>
								<source src="audio/am-jazz.mp3" type='audio/mpeg; codecs="mp3"'>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/am-jazz.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

				<article class="w25 light">
					<div class="container cover noise darker" style="background-image: url('audio/ecliptica-satellite2.jpg');">
						<div class="hashtag"><h4>#electionic</h4></div>
						<div class="player">
							<div class="groupname">ecLiptica</div>
							<div>«Satellite»</div>
							<audio id="audio6" controls>
								<source src="audio/ecliptica-satellite.mp3" type='audio/mpeg; codecs="mp3"'>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/ecliptica-satellite.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

				<article class="w25 light">
					<div class="container cover noise darker" style="background-image: url('audio/vniz-i-pryamo-nash-sneg2.jpg');">
						<div class="hashtag"><h4>#garagerock</h4></div>
						<div class="player">
							<div class="groupname">Вниз и Прямо</div>
							<div>«Наш Снег»</div>
							<audio id="audio7" controls>
								<source src="audio/vniz-i-pryamo-nash-sneg.mp3" type='audio/mpeg; codecs="mp3"'>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/vniz-i-pryamo-nash-sneg.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

				<article class="w25 light">
					<div class="container cover noise darker" style="background-image: url('audio/ivan-zubarev-chaiki2.jpg');">
						<div class="hashtag"><h4>#score</h4></div>
						<div class="player">
							<div class="groupname">Иван Зубарев</div>
							<div>«Чайки»</div>
							<audio id="audio8" controls>
								<source src="audio/ivan-zubarev-chaiki.mp3" type='audio/mpeg; codecs="mp3"'>
								<p><?=$t['browserUnsupported'][$l]?> - <a  href="audio/ivan-zubarev-chaiki.mp3" target="_blank"><?=$t['download'][$l]?></a></p>
							</audio>
						</div>
					</div>
				</article>

			</div>
			</div>
		</section>

		<section id="opinion" class="cover noise darker fixed fixed-outer">
			<div class="fixed-container">
				<div class="fixed-content">
					<h1 class="light"><?=$t['pathetic1'][$l]?></h1>
					<div class="description t-center big light"><?=$t['pathetic2'][$l]?></div>
				</div>
			</div>
		</section>

		<section id="services" class='s-black-bg noise darker thin'>
			<a href="#services" class="header"><h2 class="h1-v2 dashed gray-hov"><?=$t['myServices'][$l]?></h2></a>
			<div class="container-grid">

				<article class="w33">
					<div class="content">
						<div class="photo s-black-bg cover darker noise noise-fixed"><span>B</span></div>
						<h4><?=$t['consult'][$l]?></h4>
						<div class="price"><?=$t['free'][$l]?></div>
						<div class="text"><?=$t['consultText'][$l]?></div>
					</div>
				</article>

				<article class="w33 prime">
					<div class="content">
						<div class="photo s-black-bg cover darker noise noise-fixed"><span>A</span></div>
						<h4><?=$t['mixing'][$l]?></h4>
						<div class="text">
							<?=$t['mixingText'][$l]?>
							<br>
							<a href="#questions" class="button fancybox red-bg white center long-shadow"><?=$t['order'][$l]?></a>
						</div>
					</div>
				</article>

				<article class="w33">
					<div class="content">
						<div class="photo s-black-bg cover darker noise noise-fixed"><span>C</span></div>
						<h4><?=$t['producing'][$l]?></h4>
						<div class="price"><?=$t['priceDepends'][$l]?></div>
						<div class="text">
							<?=$t['producingText'][$l]?>
							<br>
							<a href="#questions"  class="button fancybox blue-bg light center long-shadow"><?=$t['order'][$l]?></a>
						</div>
					</div>
				</article>

			</div>
		</section>

		<section id="monials" class="light-bg noise noise-fixed">
			<a href="#monials" class="header"><h2 class="h1-v2 light-blue-hov"><?=$t['testimonials'][$l]?></h2></a>
			<div id="monials-slider" class="slick">
					<article class="monial">
						<div class="content">
							<a href="//vk.com/mo.l.odoy" target="_blank">
								<div class="photo cover half-noise" style="background-image: url('images/monials/borodin.jpg');"></div>
								<div class="person noise">
									<h4><?=$t['borodin'][$l]?></h4>
									<div class="projects">Embrace&nbsp;Eleonora, FLD-5, Fourside, Fakeberry&nbsp;Illusion, Revive&nbsp;the&nbsp;soul, K.E.Fear</div>
									<div class="clr"></div>
								</div>
							</a>
							<div class="text lighter"><?=$t['borodinText'][$l]?></div>
						</div>
					</article>

					<article class="monial">
						<div class="content">
							<a href="//vk.com/sei_ryou" target="_blank">
								<div class="photo cover half-noise" style="background-image: url('images/monials/starkov.jpg');"></div>
								<div class="person noise">
									<h4><?=$t['rich'][$l]?></h4>
									<div class="projects"><?=$t['richProjects'][$l]?></div>
									<div class="clr"></div>
								</div>
							</a>
							<div class="text lighter"><?=$t['richText'][$l]?></div>
						</div>
					</article>

					<article class="monial">
						<div class="content">
							<a href="//vk.com/x0nda" target="_blank">
								<div class="photo cover half-noise" style="background-image: url('images/monials/honda.jpg');"></div>
								<div class="person noise">
									<h4><?=$t['honda'][$l]?></h4>
									<div class="projects">Honda Band</div>
									<div class="clr"></div>
								</div>
							</a>
							<div class="text lighter"><?=$t['hondaText'][$l]?></div>
						</div>
					</article>

					<article class="monial page-2">
						<div class="content">
							<a href="//vk.com/id166820666" target="_blank">
								<div class="photo cover half-noise" style="background-image: url('images/monials/ivon.jpg');"></div>
								<div class="person noise">
									<h4><?=$t['ivon'][$l]?></h4>
									<div class="projects">Undercover</div>
									<div class="clr"></div>
								</div>
							</a>
							<div class="text lighter"><?=$t['ivonText'][$l]?></div>
						</div>
					</article>

					<article class="monial page-2">
						<div class="content">
							<a href="//vk.com/id10505836" target="_blank">
								<div class="photo cover half-noise" style="background-image: url('images/monials/sviridenko.jpg');"></div>
								<div class="person noise">
									<h4><?=$t['sviridenko'][$l]?></h4>
									<div class="projects">LetsFly</div>
									<div class="clr"></div>
								</div>
							</a>
							<div class="text lighter"><?=$t['sviridenkoText'][$l]?></div>
						</div>
					</article>

			</div>

			<?//<a href="#" id="more-monials" class="button btn-border black black-hov center">Ещё отзывы</a>?>
		</section>

		<section id="questions" class="light wood-bg-dark t-center">
			<div class="container-grid">
				<h3><?=$t['contactMe'][$l]?></h3>
				<div class="form-alternative"><?=$t['contactText'][$l]?></div>
			</div>
		</section>

<?/*
<!-- Yandex.Metrika informer
<a href="https://metrika.yandex.ru/stat/?id=36426550&amp;from=informer"
target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/36426550/3_0_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:36426550,lang:'ru'});return false}catch(e){}" /></a>
<!-- /Yandex.Metrika informer -->
*/?>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter36426550 = new Ya.Metrika({
                    id:36426550,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/36426550" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

	</body>
</html>
