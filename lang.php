<?

$t = array(

	'title' => array(
		'ru' => 'Пришло время записаться',
		'en' => 'Affordable professional mixing',
		),

	'brand' => array(
		'ru' => 'Экология Звукозаписи',
		'en' => 'Sound Organic',
		),

	'slogan' => array(
		'ru' => 'Ради музыки и добра',
		'en' => 'Healthy for your ears',
		),

	'portfolio' => array(
		'ru' => 'Портфолио',
		'en' => 'Portfolio',
		),

	'services' => array(
		'ru' => 'Услуги',
		'en' => 'Services',
		),

	'monials' => array(
		'ru' => 'Отзывы',
		'en' => 'Testimonials',
		),

	'contact' => array(
		'ru' => 'Связь',
		'en' => 'Contact',
		),

	'intro1' => array(
		'ru' => 'Пришло время<br><a href="#questions" class="dashed-3">записаться</a>',
		'en' => 'This time<br>you need<br><a href="#questions" class="dashed-3">professional mix</a>',
		),

	'intro2' => array(
		'ru' => 'Твоя музыка заслуживает профессионального сведения.',
		'en' => 'Your music deserves it.',
		),

	'order' => array(
		'ru' => 'Заказать сведение',
		'en' => 'Go for it',
		),

	'name' => array(
		'ru' => 'Павел Долгов',
		'en' => 'Pavel Dowl Dolgov',
		),

	'titles' => array(
		'ru' => 'музыкант, звукорежиссёр, мастеринг-инженер',
		'en' => 'mixing & mastering engineer, producer, musician',
		),

	'aboutme' => array(
		'ru' => '
			<p>Имею более чем десятилетний опыт, и&nbsp;качество моих работ говорит само за&nbsp;себя.</p>
			<p>Не люблю половинчатых результатов, ставлю планку очень&nbsp;высоко. Оказываю услуги лишь комплексно, чтобы быть уверенным, что&nbsp;должное внимание к&nbsp;качеству было уделено на&nbsp;каждом&nbsp;этапе.</p>
			<p>Люблю музыку и&nbsp;люблю людей. <span class="red">Ради музыки и&nbsp;добра.</span></p>
		',
		'en' => '
			<p>More than ten years of&nbsp;experience, and the quality of&nbsp;my&nbsp;work speaks for itself.</p>
			<p>Don\'t like half results, putting the bar very high. Provide muliple services in&nbsp;complex, to&nbsp;be&nbsp;sure that proper attention to&nbsp;quality was&nbsp;given on&nbsp;each stage.</p>
			<p><span class="red">Love music and love people.</span></p>
		',
		),

	'myMixing' => array(
		'ru' => 'Моё сведение',
		'en' => 'My works',
		),

	'browserUnsupported' => array(
		'ru' => 'Ваш браузер не поддерживает проигрывание аудио',
		'en' => '',
		),

	'download' => array(
		'ru' => 'Скачать',
		'en' => 'Download',
		),

	'pathetic1' => array(
		'ru' => 'Получить<br>настоящий звук<br>просто',
		'en' => 'Getting<br>real sound<br>is quite easy',
		),

	'pathetic2' => array(
		'ru' => 'Таково мнение профессионалов.',
		'en' => 'That\'s what professionals say.',
		),

	'myServices' => array(
		'ru' => 'Мои услуги',
		'en' => 'My services',
		),

	'consult' => array(
		'ru' => 'Консультации',
		'en' => 'Consulting',
		),

	'free' => array(
		'ru' => 'бесплатно',
		'en' => 'free',
		),

	'consultText' => array(
		'ru' => '
			<p>Консультирую по&nbsp;звукозаписи, сведению, настройке оборудования и&nbsp;коммутации. А также отвечу на вопросы типа:</p>
			<ul>
				<li>Как расти и развиваться начинающему, но амбициозному музыканту?</li>
				<li>Как сделать качественную запись самому в домашних условиях? (оборудование, программы, знания)</li>
				<li>Как улучшить свою аранжировку? (чтоб звучало профессионально, чтоб цепляло)</li>
				<li>Любые вопросы по моим услугам.</li>
			</ul>
			<p>Смело пиши куда тебе удобнее:
				<a href="//vk.com/gotterbild">вКонтакте</a>,
				<a href="https://chat.whatsapp.com/INyWIy3tUc2DsPIvFMPYTD" target="_blank" class="whatsapp">WhatsApp</a>,
				<a href="//telegram.me/gotterbild">Telegram</a>,
				<a href="https://www.instagram.com/pavel.dowl/">Instagram</a>,
				<a href="mailto:mail@ekozvuk.ru?subject=%D0%91%D0%B5%D1%81%D0%BF%D0%BB%D0%B0%D1%82%D0%BD%D0%B0%D1%8F%20%D0%BA%D0%BE%D0%BD%D1%81%D1%83%D0%BB%D1%8C%D1%82%D0%B0%D1%86%D0%B8%D1%8F%20ekozvuk.ru">email</a>
			</p>
			<p>И не забудь рассказать друзьям.</p>
		',
		'en' => '
			<p>Advise on&nbsp;recording, mixing, hardware&nbsp;setup and&nbsp;wiring.<br>Can answer questions like:</p>
			<ul>
				<li>How to&nbsp;make a&nbsp;quality record at&nbsp;home? (equipment, software, knowledge)</li>
				<li>How to&nbsp;improve your arrangement? (to&nbsp;sound professional, to&nbsp;hook)</li>
				<li>How to&nbsp;grow and develop if&nbsp;you\'re ambitious musician?</li>
				<li>Any questions on&nbsp;my&nbsp;services.</li>
			</ul>
			<p>Feel free to&nbsp;write where you feel comfortable:
				<a href="mailto:mail@ekozvuk.ru?subject=Hi%2C%20Pavel.%20I%27m%20for%20free%20consult%20from%20ekozvuk.ru.">email</a>,
				<a href="https://chat.whatsapp.com/INyWIy3tUc2DsPIvFMPYTD" target="_blank" class="whatsapp">WhatsApp</a>,
				<a href="//telegram.me/gotterbild">Telegram</a>,
				<a href="https://www.instagram.com/pavel.dowl/">Instagram</a>,
				<a href="//vk.com/gotterbild">VK</a>.
			</p>
			<p>Don\'t forget to tell your friends.</p>
		',
		),

	'mixing' => array(
		'ru' => 'Сведение<br>и мастеринг',
		'en' => 'Mixing & mastering',
		),

	'mixingText' => array(
		'ru' => '
			<p>Стандартная цена — <span><b>10000&nbsp;р.</b></span> за&nbsp;композицию. В&nbsp;стоимость входит:</p>
			<ul>
				<li><b>Сведение</b> на уровне западных аналогов. Включая:
					<ul>
						<li>тюнинг вокала,</li>
						<li>выравнивание инструментов по сетке,</li>
						<li>коррекция недостатков записи,</li>
						<li>эмуляция консоли.</li>
					</ul>
					Всё делается вручную с индивидуальным подходом. <span>Сведение каждого трека уникально</span>, никаких шаблонных решений!
				</li><br>
				<li><b>Мастеринг</b> в&nbsp;два мастер-трека:
					<ol>
						<li>для <span>mp3-плееров</span>, <span>компакт-дисков</span>, <span>вКонтакте</span>.</li>
						<li>для <span>iTunes</span>, <span>Youtube</span>, <span>Spotify</span>, <span>SoundCloud</span> и других сервисов, использующих технологию <span>Replay&nbsp;Gain</span>.</li>
					</ol>
					Мастер-треки предоставляются в&nbsp;форматах <mark><span>wav (44&nbsp;кГц, 16/24&nbsp;бита)</span></mark> и&nbsp;<mark><span>mp3 (320&nbsp;кб/с&nbsp;CBR)</span></mark>, по&nbsp;требованию&nbsp;&mdash; в&nbsp;любых других.
				</li>
			</ul>
			<br>
			<p><strong>Даю скидку:</strong></p>
			<ul>
				<li>Если проект несложный (до&nbsp;8&nbsp;дорожек).</li>
				<li>Если заказываешь более трёх&nbsp;композиций.</li>
				<li>Если ты студент музыкального учебного заведения, и&nbsp;запись нужна тебе для&nbsp;учебных&nbsp;целей.</li>
				<li>Если ты просто хороший человек, и&nbsp;твоя музыка несёт разумное-доброе-вечное.</li>
			</ul>
		',
		'en' => '
			<p>Standard price is&nbsp;<span><b>150&nbsp;$</b></span> for one song. It&nbsp;includes:</p>
			<ul>
				<li>Professional grade <b>mixing</b>:
					<ul>
						<li>tuning vocals,</li>
						<li>aligning to&nbsp;the grid,</li>
						<li>correction of&nbsp;deficiencies in&nbsp;the record,</li>
						<li>console emulation.</li>
					</ul>
					<span>Each mix is&nbsp;unique</span>, no&nbsp;templates and&nbsp;standard solutions!
				</li><br>
				<li><b>Mastering</b> in&nbsp;two master tracks:
					<ol>
						<li>for <span>mp3 players</span>, <span>CDs</span>, <span>social&nbsp;media</span>;</li>
						<li>for <span>iTunes</span>, <span>Youtube</span>, <span>Spotify</span>, <span>SoundCloud</span> and&nbsp;other services that use <span>Replay Gain</span> technology.</li>
					</ol>
					The master tracks are&nbsp;available in&nbsp;<mark><span>wav (44 кГц, 16/24 бита)</span></mark> and&nbsp;<mark><span>mp3 (320 кб/с CBR)</span></mark>.<br>Any other on&nbsp;demand.
				</li>
			</ul>
			<br>
			<p><strong>Discounts:</strong></p>
			<ul>
				<li>If&nbsp;your project is&nbsp;simple<br>(not more than 8 tracks).</li>
				<li>If&nbsp;you order more than three songs.</li>
				<li>If&nbsp;you\'re a&nbsp;music student and need a&nbsp;record for educational purposes.</li>
				<li>If&nbsp;you\'re a&nbsp;very good person, and&nbsp;I&nbsp;like your music very much.</li>
			</ul>
		',
		),

	'producing' => array(
		'ru' => 'Продюссирование и aранжировка',
		'en' => 'Producing & arrangement',
		),

	'priceDepends' => array(
		'ru' => 'цена договорная',
		'en' => 'price depends on your needs',
		),

	'producingText' => array(
		'ru' => '
			<ul>
				<li>Создание для твоей песни уникальной аранжировки.</li>
				<li>Корректировка существующей аранжировки.</li>
				<li>Исполнение готовой аранжировки профессиональными музыкантами.</li>
			</ul>
		',
		'en' => '
			<ul>
				<li>Creating the unique arrangement for your song from scratch.</li>
				<li>Adjustments for the existing work.</li>
				<li>Performance by professional musicians.</li>
			</ul>
		',
		),

	'testimonials' => array(
		'ru' => 'Последние отзывы',
		'en' => 'Recent testimonials',
		),

	'borodin' => array(
		'ru' => 'Кирилл<br>Бородин',
		'en' => 'Kirill<br>Borodin',
		),

	'borodinText' => array(
		'ru' => 'Первый наш EP&nbsp;писали у&nbsp;Павла. Давольно быстро работает. Очень вкусно и&nbsp;качественно обрабатывает песни легких жанров: инди, поп рок и&nbsp;прочее, отлично работает с&nbsp;акустическими песнями. Немного сложнее с&nbsp;тяжелой музыкой, но&nbsp;если объяснить, что вы&nbsp;хотите и&nbsp;выделить больше времени на&nbsp;это, то&nbsp;получите нужный вам результат.',
		'en' => 'We&nbsp;recorded our first EP&nbsp;at&nbsp;Pavel&rsquo;s place. He&nbsp;works pretty fast. Makes light genre songs very tasty and high quality: indie, pop rock and stuff, works great with acoustic songs. A&nbsp;little harder with heavy music, but if&nbsp;you&rsquo;ll put some effort to&nbsp;explain what you want, you will get a&nbsp;desired result.',
		),

	'rich' => array(
		'ru' => 'Ричард<br>Старков',
		'en' => 'Richard Starkov',
		),

	'richProjects' => array(
		'ru' => 'Jay, Шифр&nbsp;Последней&nbsp;Реальности, Площадь&nbsp;Восстания',
		'en' => 'Jay, Шифр&nbsp;Последней&nbsp;Реальности (Last&nbsp;Reality&nbsp;Cypher), Площадь&nbsp;Восстания (Revolution&nbsp;Square)',
		),

	'richText' => array(
		'ru' => 'У&nbsp;нас была плачевная ситуация&nbsp;&mdash; live запись в&nbsp;закомпрессированной комнате с&nbsp;шумами от&nbsp;телефонов. Я&nbsp;не&nbsp;думал, что из&nbsp;этого материала можно что-то сделать, но&nbsp;нет&nbsp;&mdash; Павел отлично отмастерил и&nbsp;свёл. Качественно и&nbsp;быстро. Отличная работа!',
		'en' => 'We&nbsp;had a&nbsp;bad situation&nbsp;&mdash; a&nbsp;live record in&nbsp;dirty sounding room with noise from phones. I&nbsp;did&nbsp;not think it&nbsp;is&nbsp;possible to&nbsp;do&nbsp;something with this material, but Pavel mixed and mastered it perfectly. Efficiently and quickly. Great job!',
		),

	'honda' => array(
		'ru' => 'Илья<br>Хандожко',
		'en' => 'Ilya<br>Handozhko',
		),

	'hondaText' => array(
		'ru' => 'Наша рок-группа любит работать с&nbsp;Павлом Долговым, потому что у&nbsp;него реально есть слух, он&nbsp;разбирается в&nbsp;оборудовании и&nbsp;он&nbsp;очень ответственный. [&hellip;] Звукорежиссер, звукооператор&nbsp;&mdash; это главный участник музыкального коллектива. Потому что как он&nbsp;слышит и&nbsp;хочет слышать твою музыку, именно так её слышат другие. И&nbsp;Павел очень хорошо справляется с&nbsp;этим. Поэтому с&nbsp;ним всегда очень приятно работать.',
		'en' => 'Our rock band likes to&nbsp;work with Pavel because he&nbsp;really is&nbsp;hearing, he&nbsp;knows the equipment and he&nbsp;is&nbsp;very responsible. [&hellip;] Sound designer and sound engineer is&nbsp;a&nbsp;major member of&nbsp;the musical collective. Because he&nbsp;hears your music in&nbsp;a&nbsp;way others will hear it. And Pavel is&nbsp;very good at&nbsp;it. That\'s why it&nbsp;is&nbsp;always pleasing to&nbsp;work with him.',
		),

	'ivon' => array(
		'ru' => 'Евгений<br>Ивон',
		'en' => 'Eugene<br>Ivon',
		),

	'ivonText' => array(
		'ru' => 'Я&nbsp;сам музыкант и&nbsp;музыкой занимаюсь очень давно, но&nbsp;пока на&nbsp;своем веку не&nbsp;встречал более грамотного звукаря в&nbsp;нашем городе, который был&nbsp;бы наделен таким&nbsp;же природным музыкальным чутьем, подкрепленным реальными техническими знаниями.',
		'en' => 'I&nbsp;am&nbsp;musician for a&nbsp;very long time, but so&nbsp;far in&nbsp;my&nbsp;lifetime I\'ve&nbsp;never met more competent sound engineer in&nbsp;our city, which would be&nbsp;so&nbsp;much endowed with musical feel backed by&nbsp;real technical knowledge.',
		),

	'sviridenko' => array(
		'ru' => 'Денис<br>Свириденко',
		'en' => 'Denis<br>Sviridenko',
		),

	'sviridenkoText' => array(
		'ru' => 'Паша молодец! Я&nbsp;работой доволен. Учитывая весьма умеренный ценник, Паша сработал оперативно. Свёл всё с&nbsp;учетом наших пожеланий по&nbsp;громкости и&nbsp;звучанию инструментов. В&nbsp;итоге мы&nbsp;получили достойные записи, причем в&nbsp;достаточно короткие сроки.',
		'en' => 'Pavel well done! I&rsquo;m happy with the work. Given the very moderate price tag, he&nbsp;worked quickly. He&nbsp;made everything in&nbsp;accordance with our wishes to&nbsp;balance and sound of&nbsp;instruments. In&nbsp;the end we&nbsp;got a&nbsp;decent record, and in&nbsp;a&nbsp;relatively short time.',
		),

	'contactMe' => array(
		'ru' => 'Связаться со мной',
		'en' => 'Contact me',
		),

	'contactText' => array(
		'ru' => '
			<p>Хочешь заказать мои услуги, проконсультироваться, или просто задать вопрос? <br>Тогда смело пиши на почту: <a href="mailto:mail@ekozvuk.ru?subject=%D0%94%D0%BE%D0%B1%D1%80%D1%8B%D0%B9%20%D0%B4%D0%B5%D0%BD%D1%8C!%20%D0%97%D0%B0%D0%B8%D0%BD%D1%82%D0%B5%D1%80%D0%B5%D1%81%D0%BE%D0%B2%D0%B0%D0%BB%D0%B8%20%D0%B2%D0%B0%D1%88%D0%B8%20%D1%83%D1%81%D0%BB%D1%83%D0%B3%D0%B8%20%D0%BD%D0%B0%20%D1%81%D0%B0%D0%B9%D1%82%D0%B5%20ekozvuk.ru." class="red">mail@ekozvuk.ru</a></p>
			<p class="or">или</p>
			<div class="social-mobile">
				<a rel="nofollow" href="https://vk.me/gotterbild" target="_blank" title="ВКонтакте" class="vk">
					<img src="./images/social/vk.png" alt="">
				</a>
				<a rel="nofollow" href="https://telegram.me/gotterbild" target="_blank" title="Telegram" class="telegram">
					<img src="./images/social/telegram.png" alt="">
				</a>
				<a rel="nofollow" href="https://chat.whatsapp.com/INyWIy3tUc2DsPIvFMPYTD" target="_blank" title="WhatsApp" class="whatsapp">
					<img src="./images/social/whatsapp.png" alt="">
				</a>
				<a rel="nofollow" href="https://www.instagram.com/pavel.dowl/" target="_blank" title="Instagram" class="instagram">
					<img src="./images/social/instagram.png" alt="">
				</a>
			</div>
			<div class="social">
				<a rel="nofollow" href="https://vk.me/gotterbild" target="_blank" title="ВКонтакте" class="vk"></a>
				<a rel="nofollow" href="https://telegram.me/gotterbild" target="_blank" title="Telegram" class="telegram"></a>
				<a rel="nofollow" href="https://chat.whatsapp.com/INyWIy3tUc2DsPIvFMPYTD" target="_blank" class="whatsapp"></a>
				<a rel="nofollow" href="https://www.instagram.com/pavel.dowl/" target="_blank" title="Instagram" class="instagram"></a>
			</div>
		',
		'en' => '

			<p>Want to order my services, to consult or just ask a question? <br>Then feel free to write me here: <a href="mailto:mail@ekozvuk.ru?subject=Hi.%20I%27m%20from%20ekozvuk.ru" class="red">mail@ekozvuk.ru</a></p>
			<p class="or">or</p>
			<div class="social-mobile">
				<a rel="nofollow" href="https://telegram.me/gotterbild" target="_blank" title="Telegram" class="telegram">
					<img src="./images/social/telegram.png" alt="">
				</a>
				<a rel="nofollow" href="https://chat.whatsapp.com/INyWIy3tUc2DsPIvFMPYTD" target="_blank" class="whatsapp">
					<img src="./images/social/whatsapp.png" alt="">
				</a>
				<a rel="nofollow" href="https://www.instagram.com/pavel.dowl/" target="_blank" title="Instagram" class="instagram">
					<img src="./images/social/instagram.png" alt="">
				</a>
				<a rel="nofollow" href="https://vk.me/gotterbild" target="_blank" title="VK" class="vk">
					<img src="./images/social/vk.png" alt="">
				</a>
			</div>
			<div class="social">
				<a rel="nofollow" href="https://vk.me/gotterbild" target="_blank" title="ВКонтакте" class="vk"></a>
				<a rel="nofollow" href="https://telegram.me/gotterbild" target="_blank" title="Telegram" class="telegram"></a>
				<a rel="nofollow" href="https://chat.whatsapp.com/INyWIy3tUc2DsPIvFMPYTD" target="_blank" class="whatsapp"></a>
				<a rel="nofollow" href="https://www.instagram.com/pavel.dowl/" target="_blank" title="Instagram" class="instagram"></a>
			</div>
			',
		),

	'' => array(
		'ru' => '',
		'en' => '',
		),

);

function getLang() {

	$ru_langs = array('ru','be','uk','ky','ab','mo','et','lv');

	if( $_GET['lang'] && $_GET['lang'] !== '' ):
		$lang = $_GET['lang'];
	elseif( $list = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']) ):
		$list = explode(",", $list);
		if( !preg_match("/q=/", $list[0]) ) // If no weight for the first element
			$list[0] = $list[0].";q=1";		// set it to one

		$list2 = array();
		foreach( $list as $key => $str ):
			$lng = strtolower(strtok(preg_replace("/(.+);q=(.+)/", "$1", $str), '-'));
			$q = preg_replace("/(.+)q=(.+)/", "$2", $str);
			if( $list2[$lng] < $q)
				$list2[$lng] = $q;
		endforeach;
		unset($list);

		$ru = 0;
		if( $list2["en"] && $list2["en"] >0 )
			$en = $list2["en"];

		foreach( $list2 as $lng => $q):
			if( $lng !== "en" ):
				foreach ($ru_langs as $ru_lang):
					if( $lng === $ru_lang ):
						if( $q > $ru )
							$ru = $q;
					endif;
				endforeach;
			endif;
		endforeach;

		if( $ru >= $en ):
			$lang = "ru";
		else:
			$lang = "en";
		endif;

		if( $_GET["debug"] && $_GET["debug"] !== "" ):
			echo "ru = $ru AND en = $en<br>SELECTED LANGUAGE IS: ".$lang;
		endif;

	else:
		$lang = 'en';
	endif;


	return $lang;
};

?>