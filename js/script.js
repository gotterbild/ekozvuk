$(document).ready(function() {

	// Replace all SVG images with inline SVG
	jQuery('img.svg').each(function(){
			var $img = jQuery(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			jQuery.get(imgURL, function(data) {
					// Get the SVG tag, ignore the rest
					var $svg = jQuery(data).find('svg');

					// Add replaced image's ID to the new SVG
					if(typeof imgID !== 'undefined') {
							$svg = $svg.attr('id', imgID);
					}
					// Add replaced image's classes to the new SVG
					if(typeof imgClass !== 'undefined') {
							$svg = $svg.attr('class', imgClass+' replaced-svg');
					}

					// Remove any invalid XML tags as per http://validator.w3.org
					$svg = $svg.removeAttr('xmlns:a');

					// Replace image with new SVG
					$img.replaceWith($svg);

			}, 'xml');
	});

	// Smooth scroll to anchor
	 $('a[href*=#]').bind("click", function(e){
			var anchor = $(this);
			$('html, body').stop().animate({
				 scrollTop: $(anchor.attr('href')).offset().top
			}, 1000);
			e.preventDefault();
			return false;
	 });

/*	//fancybox
	$("a[href$='.jpg'],a[href$='.png'],a[href$='.gif']").fancybox();
	$(".fancybox-inline").fancybox({
		'scrolling'		: 'no',
		'titleShow'		: false,
	});
*/
	//Audioplayer
	var audios = document.getElementsByTagName('audio'); // собираем массив тегов <audio>
	if( 	typeof audios[0].canPlayType === "function" &&
			audios[0].canPlayType("audio/mpeg;codecs=mp3") !== "")
	{ // если браузер поддерживает html5 аудио и поддерживает mp3
		for (var i=0;i<audios.length;i++){ // проходимся по каждому тегу <audio> чтобы повешать обработчик
			audios[i].addEventListener('play',function() { //вешаем обработчик на событие начала воспроизведения этого тега
				var id = this.id; // запоминаем id воспроизводимого <audio>
				for (var i=0;i<audios.length;i++){ //проходимся по каждому тегу <audio> чтобы запретить одновременное воспроизведение нескольких <audio>
					if( audios[i].id != id ){ // если рассматриваемый <audio> - не тот, на котором начато воспроизведение
						audios[i].pause(); // то жмём на паузу
					}
				} // таким образом, играет только то аудио, на котором нажали на кнопку play, а все остальные паузятся.
				$('.portfolio').find('article').removeClass('active'); // убираем активность на всех элементах
				$('#'+id).parents('article').addClass('active'); // метим воспроизводимый элемент как активный
			},false);
		}
	}else{ // если браузер НЕ поддерживает html5 аудио или НЕ поддерживает mp3

	}

	$("#monials-slider").slick({
		adaptiveHeight: true,
		arrows: true,
		appendArrows: "#monials h2",
		prevArrow: "<button class='slick-arrow'>&lsaquo;</button>",
		nextArrow: "<button class='slick-arrow'>&rsaquo;</button>",
		autoplay: true,
		autoplaySpeed: 10000,
		infinite: true,
		swipeToSlide: true,
		centerMode: true,
		centerPadding: '10%',
		slidesToShow: 3,
		responsive: [
			{
				breakpoint: 1600,
				settings: {
					arrows: true,
					infinite: true,
					slidesToShow: 2
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: false,
					infinite: true,
					slidesToShow: 1
				}
			}
		]
	});

	//Forms behavior
	$('.contactform').find('input,textarea').focusin( function() {
		$(this).attr('placeholder','');
	});
	$('.contactform').find('input,textarea').focusout( function(){
		if( $(this).val() === '' )
			$(this).attr('placeholder',$(this).attr('data-place'));
	});

	function validateEmail(value) {
		var filter =  new RegExp(/(.+)@(.+)/);
		if( filter.test(value) )
			return true;
		else
			return false;
	}

	function validateText(value) {
		var filter =  new RegExp(/(.+)/);
		if( filter.test(value) )
			return true;
		else
			return false;
	}

	//Validation
	$("input[type=email]").focusout(function(){
		var value = $(this).val();
		var submitButton = $(this).parents('form').find('input[type=submit]');
		if( validateEmail(value) ){
			$(this).removeClass('error');
		}else{
			$(this).addClass('error');
		}
	});

	$("textarea[required]").focusout(function(){
		var value = $(this).val();
		var submitButton = $(this).parents('form').find('input[type=submit]');
		if( validateText(value) ){
			$(this).removeClass('error');
		}else{
			$(this).addClass('error');
		}
	});

	$('.contactform').submit(function(event){
		event.preventDefault();
		var email = $(this).find('input[type=email]').val();
		var text = $(this).find('textarea').val();
		if( validateEmail(email) && validateText(text) )
			console.log('submited');
		else
			alert("Произошла ошибка отправки сообщения. Пожалуйста, свяжитесь со мной по почте mail@ekozvuk.ru или любым другим способом.");
	});


});